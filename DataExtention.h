#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_habits_tracker_data_ext.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IHabitsTrackerDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IHabitsTrackerDataExtention IDataExtention)

	DATA_EXTENTION_BASE_DEFINITIONS(IHabitsTrackerDataExtention, IHabitsTrackerDataExtention,
	{"name", "streak", "lastTaskId", "containerTaskId", "lastCompletedDate"})

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

	// IHabitsTrackerDataExtention interface
public:
	QString name() override
	{
		return "New habbit";
	}

	int streak() override
	{
		return 0;
	}

	int containerTaskId() override
	{
		return -1;
	}

	int lastTaskId() override
	{
		return -1;
	}

	QDate lastCompletedDate() override
	{
		return QDateTime::fromMSecsSinceEpoch(0).date();
	}
};
